package edu.boisestate.cs410.gradebook;

import java.io.IOException;

import com.budhash.cliche.Command;
import com.budhash.cliche.ShellFactory;

import edu.boisestate.cs410.gradebook.dbmanager.DataBaseConnector;

public class GradebookShell {
	private DataBaseConnector connector;
	
	/**Constructor: Creates a new instance of the database connector
	 * 
	 */
	public GradebookShell() {
		this.connector = new DataBaseConnector();
	}
	
	@Command
	public void newClass(String classId, String term, String year, String sectionNumber, String courseName) {
		this.connector.addNewClass(classId, term, year, sectionNumber, courseName);
	}
	
	@Command(name= "select-class")
	public void selectClass(String ...args) {
		this.connector.selectClass(args);
	}
	
	@Command(name="show-class")
	public void showClass(){
		this.connector.showClass();
	}
	
	@Command(name="add-category")
	public void addCategory(String name, String weight) {
		this.connector.addCategory(name, weight);
	}
	
	@Command(name="show-categories")
	public void showCategories() {
		this.connector.showCategories();
	}
	
	@Command(name="show-items")
	public void showItems() {
		this.connector.showItems();
	}
	
	@Command(name="add-items")
	public void addItem(String name, String category, String description, String points) {
		this.connector.addItem(name, category, description, points);
	}
	
	@Command(name="add-student")
	public void addStudent(String userName, String id, String name) {
		this.connector.addStudent(userName, id, name);
	}
	
	@Command(name="show-students")
	public void showStudents() {
		this.connector.showStudents();
	}
	
	@Command(name="show-students")
	public void showStudents(String pattern) {
		this.connector.showStudents(pattern);
	}
	
	public static void startShell() {
		try {
			ShellFactory.createConsoleShell("GradeBook", "", new GradebookShell()).commandLoop();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
