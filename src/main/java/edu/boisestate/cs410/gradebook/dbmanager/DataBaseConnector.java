package edu.boisestate.cs410.gradebook.dbmanager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Properties;

public class DataBaseConnector {

	private static  String url = "jdbc:postgresql://35.239.151.201:5432/course_db";
	private Connection conn;
	private Integer activeClassId = 1;
	
	public DataBaseConnector() {
		Properties props = new Properties();
		props.setProperty("user", "postgres");
		props.setProperty("password", "*Google1");
		try {
			this.conn = DriverManager.getConnection(url, props);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public DataBaseConnector(String url, String user, String password) {
		Properties props = new Properties();
		props.setProperty("user", user);
		props.setProperty("password", password);
		try {
			this.conn = DriverManager.getConnection(url, props);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void addNewClass(String classId, String term, String year, String sectionNumber, String courseName) { 
		String query = "INSERT INTO class"
				+ "(description, sectionumber, coursenumber, year, termid) VALUES"
				+ "(?,?,?,?,?)";
		PreparedStatement statement = null;
		try {
			statement = this.conn.prepareStatement(query);
			statement.setString(1, courseName);
			statement.setInt(2, Integer.parseInt(sectionNumber));
			statement.setString(3, classId);
			statement.setInt(4, Integer.parseInt(year));
			System.out.println(term);
			switch (term.toUpperCase()) {
			case "FALL":
				statement.setInt(5, 1);
				break;
			case "SPRING":
				statement.setInt(5, 2);
				break;	
			case "SUMMER":
				statement.setInt(5, 3);
				break;
			default:
				statement.setInt(5, 3);
				break;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (statement != null) {
			try {
				statement.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void showCategories() {
		String query = "SELECT * FROM category WHERE classid = ?";
		PreparedStatement statement = null;
		if (this.activeClassId == null) {
			System.out.println("Please select a class before showing a category");
		}
		else {
		try {
			statement = this.conn.prepareStatement(query);
			statement.setInt(1, this.activeClassId);
//			System.out.println(statement.toString());
			ResultSet result = statement.executeQuery();
			ResultSetMetaData rsmd = result.getMetaData();
			int columnsNumber = rsmd.getColumnCount();
			for(int i = 1; i <= columnsNumber; i++) {
				System.out.print(rsmd.getColumnName(i) + "\t\t");
			}
			System.out.println();
			while (result.next()) {
			    for (int i = 1; i <= columnsNumber; i++) {
			    	System.out.print(result.getObject(i) + "\t\t");
			    }
			    System.out.println();
			}

		}catch(SQLException e) {
			e.printStackTrace();
		}
		}
	}
	
	public void showClass() {
		String query = "SELECT * FROM class WHERE classid = ?";
		PreparedStatement statement = null;
		try {
			statement = this.conn.prepareStatement(query);
			statement.setInt(1, this.activeClassId);
//			System.out.println(statement.toString());
			ResultSet result = statement.executeQuery();
			ResultSetMetaData rsmd = result.getMetaData();
			int columnsNumber = rsmd.getColumnCount();
			for(int i = 1; i <= columnsNumber; i++) {
				System.out.print(rsmd.getColumnName(i) + "\t\t");
			}
			System.out.println();
			while (result.next()) {
			    for (int i = 1; i <= columnsNumber; i++) {
			    	System.out.print(result.getObject(i) + "\t\t");
			    }
			    System.out.println();
			}

		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void selectClass(String ...args) {
		switch (args.length) {
		case 4:
			selectClass(args[0], args[1], args[2], args[3]);
			break;
		default:
			break;
		}
	}

	public void addCategory(String categoryName, String weight) {
		String query = "INSERT INTO category"
				+ "(classid, name, weight) VALUES "
				+ "(?, ?, ?)";
		PreparedStatement statement = null;
		if(this.activeClassId == null) {
			System.out.println("Please select a class before inserting a category");
		}
		else {
		try {
			statement = this.conn.prepareStatement(query);
			statement.setInt(1, this.activeClassId);
			statement.setString(2, categoryName);
			statement.setInt(3, Integer.parseInt(weight));
			statement.executeUpdate();
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
			
		}
		}
		
	}
	

	private void selectClass(String courseNumber, String term, String year, String sectionNumber) {
		String query = "SELECT classid FROM class "
				+ " WHERE coursenumber = ? AND sectionumber = ? AND term = ? AND year = ?";
		PreparedStatement statement = null;
		try {
			statement = this.conn.prepareStatement(query);
			statement.setString(1, courseNumber);
			statement.setInt(2, Integer.parseInt(sectionNumber));
			statement.setString(3, term);
			statement.setInt(4, Integer.parseInt(year));
			System.out.println(statement.toString());
			ResultSet result = statement.executeQuery();
			ResultSetMetaData rsmd = result.getMetaData();
			int columnsNumber = rsmd.getColumnCount();
//			System.out.println(columnsNumber);
			while (result.next()) {
			    for (int i = 1; i <= columnsNumber; i++) {
			        Integer columnValue = result.getInt(i);
			        this.activeClassId = columnValue;
//			        System.out.print(columnValue + " " + rsmd.getColumnName(i));
			    }
			}

		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

	public void addItem(String name, String category, String description, String points) {
		String query = "INSERT INTO items (categoryid, name, description, pointvalue) VALUES"
				+ "(SELECT categoryid FROM category WHERE description = ? and classid = ?, ?, ?, ?)";
		
		PreparedStatement statement = null;
		try {
			statement = this.conn.prepareStatement(query);
			statement.setString(1, category);
			statement.setInt(2, this.activeClassId);
			statement.setString(3, name);
			statement.setString(4, description);
			statement.setInt(5, Integer.parseInt(points));
			System.out.println(statement.toString());
			ResultSet result = statement.executeQuery();
			ResultSetMetaData rsmd = result.getMetaData();
			int columnsNumber = rsmd.getColumnCount();
//			System.out.println(columnsNumber);
			while (result.next()) {
			    for (int i = 1; i <= columnsNumber; i++) {
			        Integer columnValue = result.getInt(i);
			        this.activeClassId = columnValue;
//			        System.out.print(columnValue + " " + rsmd.getColumnName(i));
			    }
			}

		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void addStudent(String userName, String id, String name) {
		String query = "INSERT INTO student ( studentid , username , name)"
				+ "VALUES (?, ?, ?)";
		String query2 = "INSERT INTO enrollment (studentid, classid)"
				+ "VALUES (?, ?)";
		PreparedStatement statement = null;
		if(this.activeClassId == null) {
			System.out.println("Please select a class before inserting a category");
		}
		else {
		try {
			statement = this.conn.prepareStatement(query);
			statement.setInt(1, Integer.parseInt(id));
			statement.setString(2, userName);
			statement.setString(3, name);
			statement.executeUpdate();
			statement = this.conn.prepareStatement(query2);
			statement.setInt(1, Integer.parseInt(id));
			statement.setInt(2, this.activeClassId);
			statement.executeUpdate();
			
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		}
	}
	
	public void showStudents() {
		String query = "SELECT student.* FROM student, enrollment"
				+ " WHERE student.studentid=enrollment.studentid AND enrollment.classid = ?";
		PreparedStatement statement = null;
		if(this.activeClassId == null) {
			System.out.println("Please select a class before inserting a category");
		}
		else {
		try {
			statement = this.conn.prepareStatement(query);
			
			statement.setInt(1, this.activeClassId);
			
			System.out.println(statement.toString());
			ResultSet result = statement.executeQuery();
			ResultSetMetaData rsmd = result.getMetaData();
			int columnsNumber = rsmd.getColumnCount();
//			System.out.println(columnsNumber);
			while (result.next()) {
			    for (int i = 1; i <= columnsNumber; i++) {
			        String columnValue = result.getString(i);
//			        this.activeClassId = columnValue;
			        System.out.print(columnValue + " ");
			    }
			    System.out.println();
			}
			
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		}
	}

	public void showItems() {
		String query = "SELECT items.* FROM items, category "
				+ "WHERE items.categoryid=category.categoryid AND category.classid=?";
		PreparedStatement statement = null;
		try {
			statement = this.conn.prepareStatement(query);
			statement.setInt(1, this.activeClassId);
			System.out.println(statement.toString());
			ResultSet result = statement.executeQuery();
			ResultSetMetaData rsmd = result.getMetaData();
			int columnsNumber = rsmd.getColumnCount();
//			System.out.println(columnsNumber);
			while (result.next()) {
			    for (int i = 1; i <= columnsNumber; i++) {
			        Integer columnValue = result.getInt(i);
			        this.activeClassId = columnValue;
//			        System.out.print(columnValue + " " + rsmd.getColumnName(i));
			    }
			}

		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

	public void showStudents(String pattern) {
		String query = "SELECT student.* FROM student, enrollment"
				+ " WHERE student.studentid=enrollment.studentid AND enrollment.classid = ?"
				+ " AND (student.name ILIKE ? OR student.username ILIKE ?)";
		PreparedStatement statement = null;
		if(this.activeClassId == null) {
			System.out.println("Please select a class before inserting a category");
		}
		else {
		try {
			statement = this.conn.prepareStatement(query);
			
			statement.setInt(1, this.activeClassId);
			statement.setString(2, "%"+pattern+"%");
			statement.setString(3, "%"+pattern+"%");
			
			System.out.println(statement.toString());
			ResultSet result = statement.executeQuery();
			ResultSetMetaData rsmd = result.getMetaData();
			int columnsNumber = rsmd.getColumnCount();
//			System.out.println(columnsNumber);
			while (result.next()) {
			    for (int i = 1; i <= columnsNumber; i++) {
			        String columnValue = result.getString(i);
//			        this.activeClassId = columnValue;
			        System.out.print(columnValue + " ");
			    }
			    System.out.println();
			}
			
		} catch (SQLException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		}
		
	}
}
